(function () {
  'use strict';

  angular
    .module('bothofus')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {

    menuService.addMenuItem('topbar', {
      title: 'List view',
      state: 'bothofus.listview',
      roles: ['*']
    });

    menuService.addMenuItem('topbar', {
        title: 'Map View',
        state: 'bothofus.mapview',
        roles: ['*']
      });
  }
}());
