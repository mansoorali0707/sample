(function () {
  'use strict';

  angular
    .module('bothofus.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('bothofus', {
        abstract: true,
        url: '/bothofus',
        template: '<ui-view/>'
      })
      .state('bothofus.mapview', {
        url: '/mapview',
        templateUrl: '/modules/bothofus/client/views/mapview.client.view.html',
        controller: 'mapviewController',
        controllerAs: 'vm',
        resolve:{
        	bothofusresolve:getRestaurantData,
        },
        data: {
          pageTitle: 'Map view'
        }
      })
      .state('bothofus.listview', {
        url: '/listview',
        templateUrl: '/modules/bothofus/client/views/listview.client.view.html',
        controller: 'listviewController',
        controllerAs: 'vm',
        resolve:{
        	bothofusresolve:getEventData,
        },
        data: {
          pageTitle: 'List view'
        }
      });
  }
  
  getRestaurantData.$inject = ['$http'];
  function getRestaurantData($http){
	  var url='http://www.opendatamalta.org/ckan/dataset/1d5df898-1481-42cf-85a5-673c6cdec65e/resource/73e13022-0ae6-445e-9bb5-d3d91f154a7b/download/restaurants.json&sa=D&ust=1519471932603000&usg=AFQjCNGxOVlt2otu5FQbJNrtqkCD5QnkUQ';
	  return $http.get(url).then(function(response){
		  return response;
	  });
  }
  
  getEventData.$inject = ['$http'];
  function getEventData($http){
	  var url='http://www.opendatamalta.org/ckan/dataset/dfdefba9-745e-4618-bddd-fbd179e910c8/resource/90f81009-fbc9-41ef-b031-fe3ae8bef03b/download/events.json';
	  return $http.get(url).then(function(response){
		  return response;
	  });
  }
  
}());
