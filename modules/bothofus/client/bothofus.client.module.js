(function (app) {
  'use strict';
  app.registerModule('bothofus');
  app.registerModule('bothofus.routes');
}(ApplicationConfiguration));
