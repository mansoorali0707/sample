(function () {
  'use strict';

  angular
    .module('bothofus')
    .controller('mapviewController', mapviewController);

  mapviewController.$inject = ['$scope', 'Authentication','$http','$timeout','bothofusresolve'];

  function mapviewController($scope, Authentication,$http,$timeout,bothofusresolve) {
    var vm = this;
    vm.authentication = Authentication;
    //coordinates of malta
    var lat='35.917973';
    var lng='14.409943'	
    var url='http://www.opendatamalta.org/ckan/dataset/1d5df898-1481-42cf-85a5-673c6cdec65e/resource/73e13022-0ae6-445e-9bb5-d3d91f154a7b/download/restaurants.json&sa=D&ust=1519471932603000&usg=AFQjCNGxOVlt2otu5FQbJNrtqkCD5QnkUQ';
    $http({
    	url: url,
    	method:'GET'
    }).success(function(res){
    	var restaurantJson=res;
    	var map = L.map('map', {
    	    center: [lat, lng],
    	    zoom: 13
    	});
    	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    		maxZoom: 19,
    		attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
		}).addTo(map);
    	restaurantJson.restaurants.restaurant.forEach(function(doc){
    		L.marker([doc.latitude, doc.longitude]).addTo(map).on('click',function(){			
    			var container = $('<div/>');
    			container.html('<h4 align="center"><b>'+doc.name+'</b></h4><i><b>'+doc["opening-hours"]+'</b></i><br><div>'+doc.address+'</div><br><b>TelePhone:<u>'+doc.telephone+'</u></b>');
    			this.bindPopup(container[0]);					    				    		
    		});
    	})

    	$timeout(function() {
    		map.invalidateSize();
		},500);	
    }).error(function(res){
    	console.log(res);
    });
  }
}());
