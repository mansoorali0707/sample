(function () {
  'use strict';

  angular
    .module('bothofus')
    .controller('listviewController', listviewController);

  listviewController.$inject = ['$scope', 'Authentication','$http','$timeout','bothofusresolve'];

  function listviewController($scope, Authentication,$http,$timeout,bothofusresolve) {
	  var vm=this;
	  
	  $scope.eventlist=bothofusresolve.data.EventList;
  }
}());
